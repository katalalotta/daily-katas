package org.example;

import java.util.Scanner;

public class Kata1108 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Type the string you want to convert:");
        String input = sc.next();
        System.out.println(textToNum(input));
    }

    public static void moro(){
        System.out.println("moro");
    }

    public static String textToNum(String text){
        return text.toLowerCase()
                .replaceAll("[abc]","2")
                .replaceAll("[def]","3")
                .replaceAll("[ghi]","4")
                .replaceAll("[jkl]","5")
                .replaceAll("[mno]","6")
                .replaceAll("[pqrs]","7")
                .replaceAll("[tuv]","8")
                .replaceAll("[wxyz]","9");

    }
}
